import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Hero from "./components/Hero";
import Projects from "./components/Projects";
import ContactForm from "./components/ContactForm";
import Footer from "./components/Footer";

function App() {
  const scrollToContactForm = () => {
    const contactFormSection = document.getElementById("contactFormSection");
    if (contactFormSection) {
      contactFormSection.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <div>
      <Hero scrollToContactForm={scrollToContactForm} />
      <Projects />
      <ContactForm />
      <Footer />
    </div>
  );
}

export default App;
