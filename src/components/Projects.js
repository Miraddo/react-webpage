// Projects component
import React from "react";

const Projects = () => (
  <section className="pt-4 pt-md-11 my-5">
    <div className="container">
      <div>
        <h2 className="display-5 text-center text-center">Our Projects</h2>
        <p className="lead text-center pb-0 pt-3">
          We have been providing great flooring for service
        </p>
        <p className="lead text-center pt-0 pb-3">
          problem solutions and finished the task
        </p>
        <div className="row justify-content-center">
          {Array.from({ length: 4 }, (_, i) => (
            <div key={i} className="col-md-6">
              <img
                src={process.env.PUBLIC_URL + `/images/project-0${i + 1}.png`}
                alt={`Project ${i + 1}`}
                className="rounded img-fluid"
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  </section>
);

export default Projects;
