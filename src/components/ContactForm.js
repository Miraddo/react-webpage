import React, { useState } from "react";
import { submitForm } from "../request/api";

const ErrorMessage = ({ message }) => (
  <div className="alert alert-danger" role="alert">
    {message}
  </div>
);

const SuccessMessage = () => (
  <div className="alert alert-success" role="alert">
    Message sent successfully!
  </div>
);

const ContactForm = () => {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    message: "",
  });
  const [isSuccess, setIsSuccess] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleInputChange = (fieldName, value) => {
    setFormData({ ...formData, [fieldName]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await submitForm(formData);
      if (response.ok) {
        setIsSuccess(true);
        setErrorMessage("");
      } else {
        throw new Error("Failed to send message.");
      }
    } catch (error) {
      setIsSuccess(false);
      setErrorMessage(error.message || "An error occurred.");
    }
  };

  return (
    <section className="pt-4 pt-md-11 my-5" id="contactFormSection">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <h2 className="display-5 text-center text-center">Contact Us</h2>
            {isSuccess && <SuccessMessage />}
            {errorMessage && <ErrorMessage message={errorMessage} />}
            <form onSubmit={handleSubmit} className="needs-validation">
              <div className="form-group py-2">
                <input
                  type="text"
                  className="form-control form-control-lg"
                  placeholder="Name"
                  name="name"
                  value={formData.name}
                  onChange={(e) => handleInputChange("name", e.target.value)}
                  required
                />
              </div>
              <div className="form-group py-2">
                <input
                  type="email"
                  className="form-control form-control-lg"
                  placeholder="E-Mail"
                  name="email"
                  value={formData.email}
                  onChange={(e) => handleInputChange("email", e.target.value)}
                  required
                />
              </div>
              <div className="form-group py-2">
                <textarea
                  className="form-control form-control-lg"
                  placeholder="Please ask your question here"
                  name="message"
                  value={formData.message}
                  onChange={(e) => handleInputChange("message", e.target.value)}
                  required
                ></textarea>
              </div>
              <div className="form-group py-2">
                <input
                  type="checkbox"
                  className="form-check-input"
                  id="checkBox"
                  required
                />
                <label className="form-check-label mx-3" htmlFor="checkBox">
                  I have read and accept the privacy policy.
                </label>
              </div>
              <button type="submit" className="btn btn-primary">
                Send
              </button>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContactForm;
