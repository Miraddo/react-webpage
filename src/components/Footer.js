// Footer component
import React from "react";

const Footer = () => (
  <footer
    style={{
      backgroundColor: "#5173ED",
      color: "white",
      width: "100%",
      textAlign: "center",
      padding: "20px 0",
    }}
  >
    Coding Challenge Example Webpage
  </footer>
);

export default Footer;
