// Hero component
import React from "react";

const Hero = ({ scrollToContactForm }) => (
  <section className="pt-4 pt-md-11 my-5">
    <div className="container">
      <div className="row align-items-center">
        <div className="col-12 col-md-5 col-lg-6 order-md-2">
          <img
            src={process.env.PUBLIC_URL + "/images/hero.png"}
            alt="Image"
            className="img-fluid mw-md-150 mw-lg-130 mb-6 mb-md-0"
          />
        </div>
        <div className="col-12 col-md-7 col-lg-6 order-md-1">
          <div>
            <h1 className="display-3 text-center text-md-start">
              Build Your Brand With #1 Hiring Expert{" "}
              <a href="#" className="text-primary">
                Freelancer
              </a>{" "}
              World
            </h1>
            <p className="lead text-center text-md-start text-body-secondary mb-6 mb-lg-8">
              Work with the best freelance talent from around the world on our
              secure flexible and cost-effective platform hiring expert
              freelancer world and you can contact us if you don’t understand
              about this application for your asking in here
            </p>

            <button
              className="btn btn-lg btn-primary"
              onClick={scrollToContactForm}
            >
              Contact Us
            </button>
          </div>
        </div>
      </div>
    </div>
  </section>
);

export default Hero;
