export const submitForm = async (data) => {
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json; ");

  const raw = JSON.stringify(data);

  const requestOptions = {
    method: "POST",
    headers: myHeaders,
    body: raw,
    redirect: "follow",
    mode: "no-cors", // send request without CORS
  };

  return fetch("https://cegom-api.vercel.app/api/example-form", requestOptions);
};
